package com.company;

public class SquareRootAlgorithm {
    public static double squareRoot(double number){
        double y_next = 0, y_prev = 1;
        double epsilon = 1;
        if(number < 0){
            throw new IllegalArgumentException();
        }

        if(number < 0.0001){
            return 0;
        }

        while (epsilon >= 0.0001 || epsilon <= -0.0001){
            y_next = (number / y_prev + y_prev) / 2;
            epsilon = y_prev - y_next;
            y_prev = y_next;
        }

        return y_next;
    }
}
